package org.amicofragile.katas.rover

enum class Direction {
    North {
        override fun forward(position: Position): Position {
            return Position(position.x, position.y + 1)
        }

        override fun backward(position: Position): Position {
            return Position(position.x, position.y - 1)
        }
    },
    East {
        override fun forward(position: Position): Position {
            return Position(position.x + 1, position.y)
        }

        override fun backward(position: Position): Position {
            return Position(position.x - 1, position.y)
        }
    },
    South {
        override fun forward(position: Position): Position {
            return Position(position.x, position.y - 1)
        }

        override fun backward(position: Position): Position {
            return Position(position.x, position.y + 1)
        }
    },
    West {
        override fun forward(position: Position): Position {
            return Position(position.x - 1, position.y)
        }

        override fun backward(position: Position): Position {
            return Position(position.x + 1, position.y)
        }
    };

    abstract fun forward(position: Position): Position

    abstract fun backward(position: Position): Position

    fun right() : Direction {
        return values()[(ordinal + 1) % values().size]
    }

    fun left() : Direction {
        return values()[(ordinal - 1 + values().size) % values().size]
    }
}
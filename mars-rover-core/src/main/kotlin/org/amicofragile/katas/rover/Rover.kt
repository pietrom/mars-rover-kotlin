package org.amicofragile.katas.rover

class Rover(private val planet: Planet, private var position: Position, private var direction: Direction) {
    fun position() = position
    fun direction() = direction

    fun execute(vararg commands: RoverCommand) {
        for (cmd in commands) {
            val (nextPos, nextDir) = cmd.execute(position, direction)
            val wrappedNextPos = planet.wrap(nextPos)
            position = wrappedNextPos
            direction = nextDir
        }
    }
}
package org.amicofragile.katas.rover

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class TurnRightTest {
    companion object {
        val position = Position(2, 2)
    }

    private val cmd : RoverCommand = TurnRight()

    @Test
    fun turnRightFromNorth() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.North)
        assertThat(newPosition, `is`(equalTo(position)))
        assertThat(newDirection, `is`(equalTo(Direction.East)))
    }

    @Test
    fun turnRightFromSouth() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.South)
        assertThat(newPosition, `is`(equalTo(position)))
        assertThat(newDirection, `is`(equalTo(Direction.West)))
    }

    @Test
    fun turnRightFromEast() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.East)
        assertThat(newPosition, `is`(equalTo(position)))
        assertThat(newDirection, `is`(equalTo(Direction.South)))
    }

    @Test
    fun turnRightFromWest() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.West)
        assertThat(newPosition, `is`(equalTo(position)))
        assertThat(newDirection, `is`(equalTo(Direction.North)))
    }
}
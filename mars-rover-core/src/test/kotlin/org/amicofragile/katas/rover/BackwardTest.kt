package org.amicofragile.katas.rover

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class BackwardTest {
    companion object {
        val position = Position(2, 2)
    }

    private val cmd : RoverCommand = Backward()

    @Test
    fun moveNorthForward() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.North)
        assertThat(newPosition, `is`(equalTo(Position(2, 1))))
        assertThat(newDirection, `is`(equalTo(Direction.North)))
    }

    @Test
    fun moveSouthForward() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.South)
        assertThat(newPosition, `is`(equalTo(Position(2, 3))))
        assertThat(newDirection, `is`(equalTo(Direction.South)))
    }

    @Test
    fun moveEastForward() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.East)
        assertThat(newPosition, `is`(equalTo(Position(1, 2))))
        assertThat(newDirection, `is`(equalTo(Direction.East)))
    }

    @Test
    fun moveWestForward() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.West)
        assertThat(newPosition, `is`(equalTo(Position(3, 2))))
        assertThat(newDirection, `is`(equalTo(Direction.West)))
    }
}
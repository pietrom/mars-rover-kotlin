package org.amicofragile.katas.rover

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class RoverTest {
    companion object {
        val planet = Planet("Mars", 10, 10)
    }
    @Test
    fun canMoveAndTurn() {
        val rover = Rover(planet, Position(0, 0), Direction.North)
        val commands = arrayOf(
                Forward(),
                Forward(),
                TurnRight(),
                Forward(),
                Forward(),
                TurnLeft(),
                Backward(),
                Backward()
        )
        rover.execute(*commands)
        assertThat(rover.position(), `is`(equalTo(Position(2, 0))))
    }

    @Test
    fun shouldWrapAtEdges() {
        val rover = Rover(planet, Position(8, 8), Direction.North)
        val commands = arrayOf(
                Forward(),
                Forward(), // W
                Forward(), // 8, 1
                TurnRight(),
                Forward(), // 9, 1
                Forward(), // 0, 1 W
                TurnLeft(),
                Backward(), // 0, 0
                Backward(), // 0, 9 W
                TurnLeft()
        )
        rover.execute(*commands)
        assertThat(rover.position(), `is`(equalTo(Position(0, 9))))
    }
}
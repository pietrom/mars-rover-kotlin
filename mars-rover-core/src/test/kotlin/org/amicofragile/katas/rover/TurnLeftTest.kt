package org.amicofragile.katas.rover

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class TurnLeftTest {
    companion object {
        val position = Position(2, 2)
    }

    private val cmd : RoverCommand = TurnLeft()

    @Test
    fun turnLeftFromNorth() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.North)
        assertThat(newPosition, `is`(equalTo(position)))
        assertThat(newDirection, `is`(equalTo(Direction.West)))
    }

    @Test
    fun turnLeftFromSouth() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.South)
        assertThat(newPosition, `is`(equalTo(position)))
        assertThat(newDirection, `is`(equalTo(Direction.East)))
    }

    @Test
    fun turnLeftFromEast() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.East)
        assertThat(newPosition, `is`(equalTo(position)))
        assertThat(newDirection, `is`(equalTo(Direction.North)))
    }

    @Test
    fun turnLeftFromWest() {
        val (newPosition, newDirection) = cmd.execute(position, Direction.West)
        assertThat(newPosition, `is`(equalTo(position)))
        assertThat(newDirection, `is`(equalTo(Direction.South)))
    }
}
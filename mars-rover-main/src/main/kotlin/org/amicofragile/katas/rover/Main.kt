package org.amicofragile.katas.rover

fun main() {
    val mars = Planet("Mars", 10, 10)
    val initialPos = Position(3, 3)
    val rover = Rover(mars, initialPos, Direction.North)
    val input = "F F L F F F R F F L B B L F F F F F L L F F R F F F"
    val parser = RoverCommandParser()
    val commands = parser.parse(input)
    print(rover)
    for(cmd in commands) {
        rover.execute(cmd)
        print(rover)
    }
}

fun print(rover: Rover) {
    println("POS ${rover.position()} DIR ${rover.direction()}")
}